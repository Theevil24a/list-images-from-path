function load_images_list_from_path(path){
    let list_images = []
    $.ajax({
        url: path,
        method: 'GET'
    }).done(function (data) {
        $(data).find("td > a").each(function(key,value){
            if (key != 0){
                list_images.push(path+$(this).attr('href'))
                $('.list_images').append(`<img src="${path+$(this).attr('href')}"/>`)
            }
        });    
    });
    return list_images;
}

load_images_list_from_path('./images/images_index/')